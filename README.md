MSProject - Oke Oke
=

Repositorio para el el proyecto de Modelado de Software.
-

El proyecto trata sobre una BBDD de Orquestas muy simple en la que ademas, sera posible tener un registro (otra BBDD) sobre musica, artistas y canciones. Dicho esto, lo que se busca es solo guardar la respective informacion.

Las 2 partes a diferenciar son:

	- Parte 1, entidades: Artista, Album, Cancion
	- Parte 2, entidades: Instrumentos, Orquesta, Director

Respecto a la parte 1, la idea a seguir para desarrollar esta parte del proyecto es parecido a la pagina: http://allmusic.com/ .

Seria bueno que cada uno abra una nueva tarea 

En la carpeta "documents" se encuentra toda la documentacion relacionada con el proyecto.

	- Archivo de la SRS.
	
	- Documento sobre las restricciones a tener en cuenta para el desarrollo del proyecto segun Navarro.
	
	- Ejemplo sobre como hay que hacer parte de la SRS.

###Instrucciones para la integracion entre Eclipse y Git: (En vista del reciente cambio de plataforma, vamos a esperar para hacer la integracion con Subversion y en ese momento se explicara aqui como hay que hacerlo)###

###Seguiremos usando Eclipse para hacer el desarrollo general de la aplicacion (es probable que en un future usemos NetBeans para hacer la parte grafica mas comodamente)###

#Reparto del trabajo#

Grupo Uno:

* Luis, Artista
* Dario, Album
* Mounir, Cancion

Grupo Dos:

* Alberto, Orquesta
* Oscar, Instrumentos
* Miguel, Director