\chapter{Introducci'on}
\section{Prop'osito}

El prop'osito de la SRS es facilitar y gestionar el desarrollo de una base de datos de Orquestas y m'usica en general. Este documento explica las condiciones a tener en cuenta para que el proyecto sea lo m'as parecido a lo que quiere el cliente.

Est'a dirigido para el profesor de la asignatura de Modelado de Software. %a fin de tener un documento que especifique el comportamiento del proyecto.

\section{Alcance}

La aplicaci'on se llama \emph{Oke Oke}.
Servirá para gestionar los artistas, sus álbumes y canciones que forman un sello discográfico y además gestionará una orquesta, sus instrumentos y directores.
Permite consultar información de los artistas, los álbumes que tienen, y bastante información acerca de sus canciones, que podrán ser valoradas
para  controlar el éxito que tienen. Con respecto a las orquestas, el sistema gestionara informacion sobre la organizacion de la orquestas, los instrumentos
que utilizan y el director que les dirije.
Este proyecto se desarrolla en un contexto de práctica para la asignatura de Modelado de Software.

El fin de la aplicaci'on es el tratamiento de una BBDD de orquestas o de m'usica manteniendo la integridad de la informaci'on.
\section{Definiciones, acr'onimos y abreviaciones}

SRS: “Software Requirements Specification”. Los requerimientos de los que consta el software realizado.

IEEE: Institute of Electrical and Electronics Engineers. 
	  Es la asociación técnico-profesional mundial encargada de la estandarización de las nuevas tecnologías. 
	  Es la que usaremos para elaborar el producto.

Artista: Persona o grupo de personas que crea o produce obras musicales. 

Álbum: Colección de grabaciones de sonido, en este caso canciones, creadas por el artista que tienen un tema o estilo común.

Canción: Es una composición musical para la voz humana, con letra, y comúnmente acompañada por otros instrumentos musicales.

Instrumento: Es un objeto compuesto por la combinación de uno o más sistemas resonantes y los medios para su vibración, 
			construido con el fin de reproducir sonido en uno o más tonos que puedan ser combinados por un intérprete para producir música
			
Director: Es el título de quien se encarga, en un contexto orquestal, de coordinar los distintos instrumentos que la componen.


Grupo: Se refiere a dos o más personas que a través de la voz o de instrumentos musicales, 
		transmiten una interpretación propia de obras musicales pertenecientes a diferentes géneros y estilos.

\section{Referencias}

En este documento se hace referencias al IEEE 830-1998. 
Este estándar describe las estructuras posibles, el contenido deseable y calidades de una especificación de requisitos del software.

\section{Resumen}

En esta SRS se realiza la descripción detallada del software planeado, sus funciones, la actividad usuario-programa, 
las interfaces, clarificará las restricciones y requisitos del diseño, definirá las entradas y salidas del sistema, 
los requisitos futuros y sus posibles planes de actualización previstos.