\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducci'on}{2}
\contentsline {section}{\numberline {1.1}Prop'osito}{2}
\contentsline {section}{\numberline {1.2}Alcance}{2}
\contentsline {section}{\numberline {1.3}Definiciones, acr'onimos y abreviaciones}{2}
\contentsline {section}{\numberline {1.4}Referencias}{3}
\contentsline {section}{\numberline {1.5}Resumen}{3}
\contentsline {chapter}{\numberline {2}Descripci'on General}{4}
\contentsline {section}{\numberline {2.1}Perspectiva del producto}{4}
\contentsline {section}{\numberline {2.2}Funciones del producto}{4}
\contentsline {section}{\numberline {2.3}Caracter'isticas del usuario}{5}
\contentsline {section}{\numberline {2.4}Restricciones}{6}
\contentsline {section}{\numberline {2.5}Supuestos y dependencias}{6}
\contentsline {chapter}{\numberline {3}Requisitos Espec'ificos}{7}
\contentsline {section}{\numberline {3.1}Interfaces externas}{7}
\contentsline {section}{\numberline {3.2}Funciones}{7}
\contentsline {section}{\numberline {3.3}Requisitos de rendimiento}{18}
\contentsline {section}{\numberline {3.4}Requisitos l'ogicos de la BBDD}{18}
\contentsline {section}{\numberline {3.5}Restricciones de dise'no}{18}
